from selenium.webdriver import ActionChains
from selenium.webdriver.remote.webelement import WebElement
from selenium.webdriver.support.select import Select


class CommandsFactory:

    def __init__(self, driver, element: WebElement):
        self.driver = driver
        self.element = element

    def select_by_text(self, text):
        Select(self.element).select_by_visible_text(text)

    def select_by_index(self, index):
        Select(self.element).select_by_index(index)

    def select_by_value(self, value):
        Select(self.element).select_by_value(value)

    def get_list_item_count(self):
        return len(Select(self.element).options)

    def get_all_list_item(self):
        select = Select(self.element)
        list_item = []
        for item in select.options:
            list_item.append(item.text)
        return list_item

    def get_list_selected_item(self):
        select = Select(self.element)
        list_item = []
        for item in select.all_selected_options:
            list_item.append(item.text)
        return list_item

    def verify_list_item(self, text):
        return text in self.get_all_list_item()

    def double_click(self):
        ActionChains(self.element.parent).double_click(self.element).perform()
        return self

    def right_click(self):
        ActionChains(self.element.parent).context_click(on_element=self.element)
        return self

    def click_and_hold(self):
        ActionChains(self.element.parent).click_and_hold(on_element=self.element)
        return self

    def release(self):
        ActionChains(self.element.parent).release(on_element=self.element)
        return self

    def hover(self):
        ActionChains(self.element.parent).move_to_element(self.element).perform()
