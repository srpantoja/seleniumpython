from selenium.webdriver.remote.webelement import WebElement

class JavascriptFactory:

    def __init__(self, driver, element: WebElement):
        self._driver = driver
        self._element = element

    def click(self):
        self._driver.execute_script("arguments[0].click();", self._element)

    def move_to_center(self) -> WebElement:
        desired_y = (self._element.size['height'] / 2) + self._element.location['y']
        window_h = self._driver.execute_script('return window.innerHeight')
        window_y = self._driver.execute_script('return window.pageYOffset')
        current_y = (window_h / 2) + window_y
        scroll_y_by = desired_y - current_y
        self._driver.execute_script("window.scrollBy(0, arguments[0]);", scroll_y_by)
        return self._element

    def move_to(self) -> WebElement:
        self._driver.execute_script("arguments[0].scrollIntoView();", self._element)
        return self._element

