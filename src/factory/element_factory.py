from typing import List

from selenium.webdriver.remote.webelement import WebElement

from src.factory.commands_factory import CommandsFactory
from src.factory.javascript_factory import JavascriptFactory
from src.utils.env import Env


class ElementFactory:

    def __init__(self, driver, element: WebElement):
        self._driver = driver
        self._element = element
        self._highlight_web_element()

    def and_(self) -> WebElement:
        return self._element

    def and_find(self, *locator):
        self._element = self._element.find_element(*locator)
        return self

    def and_find_all(self, *locator) -> List['ElementFactory']:
        elements = self._driver.find_elements(*locator)
        elements_factory: List[ElementFactory] = []
        for element in elements:
            elements_factory.append(ElementFactory(self._driver, element))
        return elements_factory

    def and_run_js(self) -> JavascriptFactory:
        return JavascriptFactory(self._driver, self._element)

    def and_run_action(self) -> CommandsFactory:
        return CommandsFactory(self._driver, self._element)

    def _highlight_web_element(self):
        if Env().get_env("highlight").upper() == "TRUE":
            self._driver.execute_script("arguments[0].style.border='2px ridge #33ffff'", self._element)
