from typing import Tuple, List

from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

from src.factory.by_factory import BY
from src.factory.element_factory import ElementFactory
from src.utils.env import Env


class WaitFactory:

    def __init__(self, driver):
        self._driver = driver
        self.wait = WebDriverWait(self._driver, Env().get_env("wait_timeout"))

    def is_visible(self, loc: BY) -> ElementFactory:
        locator = loc.by, loc.locator
        self.wait.until(EC.visibility_of_element_located(locator))
        return ElementFactory(self._driver, self._driver.find_element(*locator))

    def is_clickable(self, loc: BY) -> ElementFactory:
        locator = loc.by, loc.locator
        self.wait.until(EC.element_to_be_clickable(locator))
        return ElementFactory(self._driver, self._driver.find_element(*locator))

    def url_to_be(self, url):
        self.wait.until(EC.url_to_be(url))

    def url_contains(self, url):
        self.wait.until(EC.url_contains(url))
        
    def all_is_visible(self, loc: BY):
        locator = loc.by, loc.locator
        self.wait.until(EC.visibility_of_all_elements_located(locator))
        elements = self._driver.find_elements(*locator)
        elements_factory: List[ElementFactory] = []
        for element in elements:
            elements_factory.append(ElementFactory(self._driver, element))
        return elements_factory

