from faker import Faker

from src.data.models.register import Register


class RegisterGenerator:
    f = Faker('pt_BR')

    def with_valid_data(self):
        return Register(
            self.f.name(),
            self.f.email(),
            self.f.password(length=8, special_chars=False, digits=True, upper_case=True, lower_case=True)
        )
