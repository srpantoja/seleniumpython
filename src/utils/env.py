import os

from dotenv import load_dotenv


class Env:

    @staticmethod
    def get_env(key):
        load_dotenv()
        return os.getenv(key)
