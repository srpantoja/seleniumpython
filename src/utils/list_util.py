from typing import List

from src.factory.by_factory import BY
from src.factory.element_factory import ElementFactory


def web_element_list_to_element_factory_list(driver, loc: BY) -> List[ElementFactory]:
    elements = driver.find_elements(loc.by, loc.locator)
    elements_factory: List[ElementFactory] = []
    for element in elements:
        elements_factory.append(ElementFactory(driver, element))
    return elements_factory
