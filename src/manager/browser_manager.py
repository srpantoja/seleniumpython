from webbrowser import Chrome

from selenium import webdriver

from src.utils.env import Env

from selenium.webdriver.chrome.options import Options as chrome_options
from selenium.webdriver.firefox.options import Options as firefox_options
from selenium.webdriver.edge.options import Options as edge_options


class BrowserManager:
    def __init__(self):
        self._browser = Env().get_env("browser")
        self._headless = Env.get_env("headless")

    def create_local_driver(self):
        if "CHROME" == self._browser:
            return webdriver.Chrome(self.get_chrome_options())
        if "FIREFOX" == self._browser:
            return webdriver.Firefox(self.get_firefox_options())
        if "EDGE" == self._browser:
            return webdriver.Edge(self.get_edge_options())
        return webdriver.Chrome(self.get_chrome_options())

    def create_remote_driver(self):
        grid_options = Env().get_env("grid_url")
        if "CHROME" == self._browser:
            return webdriver.Remote(command_executor=grid_options, options=self.get_chrome_options())
        if "FIREFOX" == self._browser:
            return webdriver.Remote(command_executor=grid_options, options=self.get_firefox_options())
        if "EDGE" == self._browser:
            return webdriver.Remote(command_executor=grid_options, options=self.get_edge_options())
        return webdriver.Remote(command_executor=grid_options, options=self.get_chrome_options())

    def get_chrome_options(self):
        options = chrome_options()
        if self._headless.upper() == "TRUE":
            options.add_argument("--headless=new")
        options.add_argument("--enable-chrome-browser-cloud-management")
        return options

    def get_firefox_options(self):
        options = firefox_options()
        if self._headless == "TRUE":
            options.add_argument("-headless")

        return options

    def get_edge_options(self):
        options = edge_options()
        if self._headless == "TRUE":
            options.add_argument("--headless=new")
        return options
