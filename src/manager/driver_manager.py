from src.manager.browser_manager import BrowserManager
from src.utils.env import Env

class DriverManager:

    @staticmethod
    def create_instance():
        host = Env().get_env("host")
        if host == "LOCAL":
            driver = BrowserManager().create_local_driver()
            driver.maximize_window()
            return driver
        if host == "REMOTE":
            return BrowserManager().create_remote_driver()
