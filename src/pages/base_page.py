from selenium import webdriver

from typing import List, Union

import allure

from src.factory.by_factory import BY
from src.factory.element_factory import ElementFactory
from src.factory.wait_factory import WaitFactory
from src.utils.list_util import web_element_list_to_element_factory_list


class BasePage:

    def __init__(self, driver: Union[webdriver.Chrome, webdriver.Firefox, webdriver.Edge]):
        self._driver: webdriver = driver
        self.wait = WaitFactory(driver)

    def s(self, loc: BY):
        return ElementFactory(self._driver, self._driver.find_element(loc.by, loc.locator))

    def ss(self, loc: BY) -> List[ElementFactory]:
        return web_element_list_to_element_factory_list(self._driver, loc)

    @allure.step("access the page")
    def open(self, url):
        self._driver.get(url)
        self.wait.url_contains(url)
        self._take_screenshot()
        return self

    def _take_screenshot(self):
        png_bytes = self._driver.get_screenshot_as_png()
        allure.attach(
            png_bytes,
            name="full-page",
            attachment_type=allure.attachment_type.PNG
        )
        return
