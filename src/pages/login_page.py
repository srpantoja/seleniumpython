import allure
from selenium.webdriver.common.by import By

from src.factory.by_factory import BY
from src.pages.base_page import BasePage


class LoginPage(BasePage):

    def __init__(self, driver):
        super().__init__(driver)
        self.driver = driver

    btn_cadastrar = BY(By.XPATH, '//a[@data-testid="cadastrar"]')

    @allure.step("click in register button on login page")
    def click_on_register_btn(self):
        self.wait.is_visible(self.btn_cadastrar).and_().click()
        self._take_screenshot()
        return self