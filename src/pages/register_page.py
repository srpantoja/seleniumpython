from time import sleep

import allure
from selenium.webdriver.common.by import By

from src.data.models.register import Register
from src.data.validations.register_validations import RegisterValidations
from src.factory.by_factory import BY
from src.pages.base_page import BasePage


class RegisterPage(BasePage):

    def __init__(self, driver):
        super().__init__(driver)
        self.driver = driver

    # inputs
    inp_nome = BY(By.ID, 'nome')
    inp_email = BY(By.ID, 'email')
    inp_senha = BY(By.ID, 'password')

    # labels
    lbl_err_nome_obrigatorio = BY(By.XPATH, '//span[text()="Nome é obrigatório"]')
    lbl_list_err = BY(By.XPATH, '//div[@class="alert alert-secondary alert-dismissible"]/span')

    # buttons
    btn_cadastrar = BY(By.XPATH, '//button[@data-testid="cadastrar"]')

    @allure.step("click in register button on register page")
    def click_register_btn(self):
        self.wait.is_visible(self.btn_cadastrar).and_().click()
        self._take_screenshot()
        return self

    @allure.step("fill register form")
    def fill_register_form(self, data: Register):
        self.wait.is_visible(self.inp_nome).and_().send_keys(data.nome)
        self.s(self.inp_email).and_().send_keys(data.email)
        self.s(self.inp_senha).and_().send_keys(data.senha)
        self._take_screenshot()
        return self

    def register(self):
        self.wait.is_visible(self.inp_nome)
        self.s(self.inp_nome).and_().send_keys("teste")
        return self

    def then_register_page_should_error_message_on_label_name(self):
        self.click_register_btn()
        lbl_nome_obrigatorio = (self.wait
                                .is_visible(self.lbl_err_nome_obrigatorio)
                                .and_()
                                .text)

        self._take_screenshot()
        assert lbl_nome_obrigatorio == "Nome é obrigatório"
        return self

    @allure.step("system should show a list of error message")
    def the_register_page_should_list_of_error_msg(self):
        self.click_register_btn()

        list_lbl = self.wait.all_is_visible(self.lbl_list_err)

        self._take_screenshot()
        for lbl in list_lbl:
            assert lbl.and_().text in RegisterValidations().mandatory
        return self
