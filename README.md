Projeto Python com Selenium
Este projeto é um exemplo básico de automação de testes usando Selenium em Python. Ele inclui testes automatizados para interagir com uma aplicação web.

# Pré-requisitos
Para executar este projeto, você precisa ter os seguintes pré-requisitos instalados:

1. Python (versão 3.6 ou superior)

# Instalação
Instale as dependências do projeto:
```bash
 pip install -r requirements.txt
 ```
# Configuração do Ambiente
Antes de executar os testes, é necessário configurar algumas variáveis de ambiente. Crie um arquivo .env na raiz do projeto e configure as seguintes variáveis:

```bash
host = LOCAL
browser = CHROME
headless = true
highlight = true
wait_timeout = 30
grid_url = http://localhost:4444
base_url = https://front.serverest.dev
```
Aqui está uma breve explicação de cada variável:

1. **host**: Define o host de execução dos testes (por exemplo, LOCAL, REMOTE).
2. **browser**: Define o navegador a ser utilizado para os testes (por exemplo, CHROME, FIREFOX).
3. **headless**: Define se o navegador será executado em modo headless (sem interface gráfica).
4. **highlight**: Define se os elementos HTML interagidos pelos testes serão destacados visualmente.
5. **wait_timeout**: Define o tempo máximo de espera para localização de elementos na página.
6. **grid_url**: Define a URL do Selenium Grid, se estiver usando um.
7. **base_url**: Define a URL base da aplicação web a ser testada.

# Execução dos Testes
Para executar os testes, utilize o seguinte comando:

```bash
pytest -v
ou 
python -m pytest
ou
python -m pytest --alluredir allure-results
```
Este comando executará os testes usando o framework pytest. Os resultados serão exibidos no terminal.

# Execução do allure Report
```bash
allure-cli/bin/allure serve allure-results
```
Este comando irá executar o allure serve e abrirá em seu navegador o resultado dos testes

# Principais Dependências

1. **selenium (4.19.0)**: Biblioteca para automação de testes web.
2. **pytest (8.1.1)**: Framework de teste para Python.
3. **Faker (24.9.0)**: Biblioteca para geração de dados falsos para testes.
4. **python-dotenv (1.0.1)**: Biblioteca para carregar variáveis de ambiente de um arquivo .env.
5. **pytest-xdist (3.5.0)**: Plugin pytest para execução de testes em paralelo.
6. **allure-pytest (2.13.5)**: Plugin para gerar relatórios de teste com Allure.

# Arquitetura do projeto

```bash
src/
├── data/
│   ├── generator/
│   ├── models/
│   └── validations/
├── factory/
│   ├── by_factory.py
│   ├── commands_factory.py
│   ├── element_factory.py
│   ├── javascript_factory.py
│   └── wait_factory.py
├── manager/
│   ├── driver_manager.py
│   └── browser_manager.py
├── pages/
│   └── base_page.py
│   └── ... 
└── utils/
    └── env.py
    └── ... 
```

1. **data/**:Esta pasta contém módulos relacionados ao gerenciamento de dados para os testes.
   1. **generator/**: Contém classes ou funções para gerar dados falsos usando a biblioteca Faker.
   2. **models**: Define modelos de dados ou classes para representar entidades específicas do domínio.
   3. **validations/**: Fornece funções ou classes para validar dados ou comportamentos.
2. **factory/**: Esta pasta contém modulos relacionados a comandos úteis para reduzir a escrita e diminuir a complexidade dos testes.
   1. **by_factory.py**: Classe responsável por receber o By e o locator do elemento. 
   2. **commands_factory.py**: Contém métodos personalizados para serem executados, como select, action e entre outros.
   3. **element_factory.py**: Contém métodos que retornam os objetos: commands_factory, javascript_factory e web_element
   4. **javascript_factory.py**: Contém métodos personalizados para executar script javascript como: click, move_to e entre outros.
   5. **wait_factory.py**: Contém métodos relacionados a esperas como: esperar ser visível, esperar ser clicável e sempre retornam element_factory
3. **manager/**: Aqui estão os módulos responsáveis por gerenciar recursos relacionados à automação, como o WebDriver e o navegador.
   1. **driver_manager.py**: Fornece funcionalidades para gerenciar a inicialização e encerramento do WebDriver.
   2. **browser_manager.py**: Contém métodos para configurar e controlar o navegador, como iniciar uma nova instância, fechar, maximizar a janela etc.
4. **pages/**: Esta pasta contém classes que representam as páginas da aplicação web sob teste.
   1. **base_page.py**: Define a classe base para todas as páginas, contendo métodos comuns que são utilizados em várias páginas.
5. **utils/**: Aqui estão módulos utilitários que podem ser usados em todo o projeto.
    1. **env.py**: Fornece funcionalidades para lidar com variáveis de ambiente, como carregar variáveis do arquivo .env.