import allure
import pytest
from selenium import webdriver
from selenium.webdriver.common.by import By

from src.factory.by_factory import BY
from src.manager.driver_manager import DriverManager
from src.pages.register_page import RegisterPage
from src.utils.env import Env
from tests.test_base import TestBase


class TestWait(TestBase):

    @allure.title("Teste de arquitetura wait is visible")
    def test_wait_is_visible(self):
        self.pg_login.open(self.base_url).click_on_register_btn()
        self.pg_register.then_register_page_should_error_message_on_label_name()

    @allure.title("Teste de arquitetura wait all is visible")
    def test_wait_all_is_visible(self):
        self.pg_login.open(self.base_url).click_on_register_btn()
        self.pg_register.the_register_page_should_list_of_error_msg()

    def test_locator(self):
        loc = BY(By.XPATH, "teste")
        print(loc.by, loc.locator)
        print(type(loc.by))
        print(isinstance(loc.by, str))  # Saída: True


if __name__ == "__main__":
    pytest.main()
