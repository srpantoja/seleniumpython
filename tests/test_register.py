import pytest

from src.data.generator.register_generator import RegisterGenerator
from tests.test_base import TestBase


class TestRegister(TestBase):

    def test_register(self):
        model_register = RegisterGenerator().with_valid_data()

        (self.pg_login
         .open(self.base_url)
         .click_on_register_btn())

        (self.pg_register
         .fill_register_form(model_register)
         .click_register_btn())


if __name__ == "__main__":
    pytest.main()
