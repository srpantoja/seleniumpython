from src.manager.driver_manager import DriverManager
from src.pages.login_page import LoginPage
from src.pages.register_page import RegisterPage
from src.utils.env import Env


class TestBase:

    base_url = Env().get_env("base_url")

    @classmethod
    def setup_class(cls):
        cls.driver = DriverManager().create_instance()
        cls.pg_register = RegisterPage(cls.driver)
        cls.pg_login = LoginPage(cls.driver)

    @classmethod
    def teardown_class(cls):
        cls.driver.quit()
